import React, { useState } from "react";
import ReplyForm from "./ReplyForm";

export const Post = (props) => {
  let { post } = props;
  const [postComment, setPostComment] = useState(post.comment);
  const [replies, setReplies] = useState([]);
  const [showForm, setShowForm] = useState(false);
  const [editComment, setEditComment] = useState(false);

  function addReply(post) {
    setReplies((p) => {
      return [...p, post];
    });
  }

  function deleteReply(replyPost) {
    console.log("old replies", replies);
    setReplies((p) => {
      let updatedReplies = p.filter((reply) => reply.name !== replyPost.name);
      return updatedReplies;
    });
  }

  return (
    <>
      <div className="post">
        <h5 className="name">{post.name}</h5>
        {!editComment && <h6 className="post">{postComment}</h6>}
        {editComment && (
          <input
            type="text"
            value={postComment}
            onChange={(e) => {
              if (e.target.value.trim() !== "") setPostComment(e.target.value);
            }}
            // onBlur={() => setEditComment(false)}
          />
        )}

        <div className="btns">
          <button
            className="reply"
            onClick={() => {
              setShowForm(true);
            }}
            disabled={editComment}
          >
            Reply
          </button>
          <button
            className="edit"
            onClick={() => {
              if (editComment) {
                setEditComment(false);
              } else setEditComment(true);
            }}
          >
            {editComment ? "Save" : "Edit"}
          </button>
          <button
            className="reply"
            onClick={() => props.deleteReply(post)}
            disabled={editComment}
          >
            Delete
          </button>
        </div>

        {showForm && (
          <ReplyForm addReply={addReply} setShowForm={setShowForm} />
        )}
      </div>

      <div className="replies ms-5">
        {replies?.map((reply, index) => (
          <Post
            key={post.name + index}
            post={reply}
            deleteReply={deleteReply}
          />
        ))}
      </div>
    </>
  );
};

export default Post;
