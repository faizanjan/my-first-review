import {useState} from "react";

export const ReplyForm = ({addReply, setShowForm}) => {
    const [replyName, setReplyName] = useState("");
    const [replyComment, setReplyComment] = useState("");
    
    const handleReplySubmit =(e)=>{
        e.preventDefault();
        let post = {
            name:replyName,
            comment: replyComment
        }
        addReply(post)
    }
  return (
    <form
      className="d-flex flex-column w-25"
      onSubmit={e=>handleReplySubmit(e)}
    >
      <input type="text" placeholder="Your Name" value={replyName} onChange={(e)=>setReplyName(e.target.value)}/>
      <textarea placeholder="comment" value={replyComment} onChange={e=>setReplyComment(e.target.value)}/>
      <div className="btns">
        <button type="submit"> Post </button>
        <button
          onClick={() => {
            setShowForm(false);
          }}
        >
          {" "}
          Cancel{" "}
        </button>
      </div>
    </form>
  );
};

export default ReplyForm;
