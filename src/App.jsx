import Post from "./components/post";

function App() {

  let initPost = {
    name: "Faizan",
    comment: " This is the first Post"
  }

  return (
    <>
     <Post post={initPost}/>
    </>
  );
}

export default App;
